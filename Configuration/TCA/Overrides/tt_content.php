<?php
defined('TYPO3_MODE') or die();

call_user_func(function() {
    $languageFilePrefix = 'LLL:EXT:fluid_styled_content/Resources/Private/Language/Database.xlf:';
    $frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem('tt_content', 'CType',
        array('TVW', '--div--')
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'Kontakt',
            'tvw_contact',
            'mimetypes-x-content-login'
        ]
    );

    $GLOBALS['TCA']['tt_content']['types']['tvw_contact'] = [
        'showitem'         => '
            --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
            --palette--;' . $frontendLanguageFilePrefix . 'palette.header;header,
            pi_flexform,
            assets
    '
    ];
    $GLOBALS['TCA']['tt_content']['types']['tvw_contact']['columnsOverrides'] = [
        'assets' => [
            'config' => [
                'maxitems' => 1
            ]
        ]
    ];

    // Add FlexForm
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '',
        'FILE:EXT:tvw_website_distribution/Configuration/FlexForms/tvw_contact_flexform.xml',
        'tvw_contact'
    );
});