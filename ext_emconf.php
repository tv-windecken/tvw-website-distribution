<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'TVW Website Distribution',
	'description' => '',
	'category' => 'distribution',
	'author' => 'Matthias Eichholz',
	'author_email' => 'mail@matthias-eichholz.de',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.0-7.6.99'
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);