'use strict';

import fs from 'fs';
import gulp from 'gulp';
import plugins from 'gulp-load-plugins';
import rimraf from 'rimraf';
import yaml from 'js-yaml';
import yargs from 'yargs';

const $ = plugins();

const PRODUCTION = !!(yargs.argv.production);

const {COMPATIBILITY, UNCSS_OPTIONS, PATHS} = loadConfig();

function loadConfig() {
    let ymlFile = fs.readFileSync('config.yml', 'utf8');
    return yaml.load(ymlFile);
}

function clean(done) {
    rimraf(PATHS.dist + '/Styles', done);
}

function sass() {
    return gulp.src('./Scss/tvw.scss')
        .pipe($.sass({
            includePaths: PATHS.sass
        }))
        .pipe($.autoprefixer({
            browsers: COMPATIBILITY
        }))
        .pipe($.if(PRODUCTION, $.uncss(UNCSS_OPTIONS)))
        .pipe($.if(PRODUCTION, $.cssnano()))
        .pipe(gulp.dest(PATHS.dist + '/Styles'));
}
function watch() {
    gulp.watch('./Scss/**/*.scss', sass);
}

gulp.task('default', gulp.series(sass, watch));
