/**
*  jquery.slidemenu
*  Developed by Matthias Eichholz (www.matthias-eichholz.de)
*
*  Copyright (C) 2010 Matthias Eichholz
*/
(function($) {
$.fn.extend({        
  slidemenu: function(options) {
    
    var defaults = {
      iconSize: 72,
      labelSize: 300,
      defaultSlide: 0,
      slideEvent: 'mouseover',
      duration: 250        
    };
    
    var config = $.extend(defaults, options);
    
    return this.each(function() {
      var container = $(this);
      var slides = container.children('li');
      
      var preCalculatedLeft = [];
      for (var i = 0; i < slides.size(); i++) {
        preCalculatedLeft[i] = [];
        for (var j = 0; j < slides.size(); j++) {
          if (j === 0) {
            preCalculatedLeft[i][j] = 0;
          } else if (i === j) {
            preCalculatedLeft[i][j] = (j - 1) * config.iconSize;
          } else {
            preCalculatedLeft[i][j] = (j <= i) ? (j-1) * config.iconSize : (j-1) * config.iconSize + config.labelSize;
          }
        }
      }
      
      if (config.defaultSlide !== 0) {
        slides.each(function(i) {
          slides.eq(i).css('left', preCalculatedLeft[config.defaultSlide][i]);
        });
      }
      
      slides.each(function(i) {
        var slide = $(this);
        if (i !== 0) {
          slide.bind(config.slideEvent, function() {
            var previousLeft = [];
            slides.each(function(i) {
              previousLeft[i] = slides.eq(i).position().left;
            });
            slides.stop().removeClass('active');
            var distance = previousLeft[i] - preCalculatedLeft[i][i];
            if (slide.position().left != preCalculatedLeft[i][i]) {
              slide.addClass('active').animate({left: preCalculatedLeft[i][i]}, {
                duration: config.duration,
                step: function(now) {
                  var percentage = (distance !== 0) ? 1 - (now - preCalculatedLeft[i][i])/distance : 1;
                  slides.each(function(j) {
                    if (j > 0 && i != j) {
                      slides.eq(j).css('left', previousLeft[j] - ((previousLeft[j] - preCalculatedLeft[i][j]) * percentage));
                    }
                  });
                }
              });
            } else {
              distance = preCalculatedLeft[i][i + 1] - previousLeft[i + 1];
              slides.eq(i + 1).animate({left: preCalculatedLeft[i][i + 1]}, {
                duration: config.duration,
                step: function(now) {
                  var percentage = (distance !== 0) ? 1 - (preCalculatedLeft[i][i + 1] - now)/distance : 1;
                  slides.each(function(j) {
                    if (j > 0 && i != j) {
                      slides.eq(j).css('left', previousLeft[j] - ((previousLeft[j] - preCalculatedLeft[i][j]) * percentage));
                    }
                  });
                }
              });  
            }            
          });
        }
      });
      container.bind('mouseleave', function() {
        var previousLeft = [];
        slides.each(function(i) {
          previousLeft[i] = slides.eq(i).position().left;
        });
        

        if ((previousLeft[config.defaultSlide] == preCalculatedLeft[config.defaultSlide][config.defaultSlide]) && (config.defaultSlide < (slides.size() - 1))) {
          var distance = Math.abs(preCalculatedLeft[config.defaultSlide][config.defaultSlide + 1] - previousLeft[config.defaultSlide + 1]);
          slides.eq(config.defaultSlide + 1).animate({left: preCalculatedLeft[config.defaultSlide][config.defaultSlide + 1]},{
            duration: config.duration,
            step: function(now) {
              var percentage = (distance !== 0) ? 1 - (preCalculatedLeft[config.defaultSlide][config.defaultSlide + 1] - now)/distance : 1;
              slides.each(function(i) {
                if (i > config.defaultSlide) {
                  slides.eq(i).css('left', previousLeft[i] - ((previousLeft[i] - preCalculatedLeft[config.defaultSlide][i]) * percentage));
                }
              });
            }
          });
        } else {
          var dist = Math.abs(preCalculatedLeft[config.defaultSlide][config.defaultSlide] - previousLeft[config.defaultSlide]);
          slides.eq(config.defaultSlide).animate({left: preCalculatedLeft[config.defaultSlide][config.defaultSlide]},{
            duration: config.duration,
            step: function(now) {
              var percentage = (dist !== 0) ? 1 - (now - preCalculatedLeft[config.defaultSlide][config.defaultSlide])/dist : 1;
              slides.each(function(i) {
                if (i < config.defaultSlide) {
                  slides.eq(i).css('left', previousLeft[i] - ((previousLeft[i] - preCalculatedLeft[config.defaultSlide][i]) * percentage));
                }
              });
            }
          });
        }
      });
    });
  }
});
})(jQuery);