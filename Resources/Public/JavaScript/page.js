$(document).ready(function() {
  var form = jQuery('#search > form');
  var field = form.children('.text');
  field.bind('focusin', function() {
    field.val('');
  }).bind('focusout', function() {
    if (field.val().length === 0) {
      field.val('Suche...');
    }
  });
  
  var name = jQuery('#contact-name');
  name.bind('focusin', function() {
    if (name.val() == 'Name') {
      name.val('');
    }
  }).bind('focusout', function() {
    if (name.val().length === 0) {
      name.val('Name');
    }
  });
  var email = jQuery('#contact-email');
  email.bind('focusin', function() {
    if (email.val() == 'Email') {
      email.val('');
    }
  }).bind('focusout', function() {
    if (email.val().length === 0) {
      email.val('Email');
    }
  });
  var message = jQuery('#contact-message');
  message.bind('focusin', function() {
    if (message.val() == 'Nachricht') {
      message.val('');
    }
  }).bind('focusout', function() {
    if (message.val().length === 0) {
      message.val('Nachricht');
    }
  });
  
  $('.fancybox').fancybox();
});

$(function() {
	$('img.lazy').lazyload();
});