<?php
namespace TVW\WebsiteDistribution\Hooks;


use TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ContactPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    /**
     * Preprocesses the preview rendering of a content element.
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
     * @param bool $drawItem Whether to draw the item using the default functionalities
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     * @return void
     */
    public function preProcess(PageLayoutView &$parentObject, &$drawItem, &$headerContent, &$itemContent, array &$row)
    {
        if ($row['CType'] === 'tvw_contact') {
            $flexform = GeneralUtility::xml2array($row['pi_flexform']);
            if (is_array($flexform['data']['sDEF']['lDEF'])) {
                $flexform = $flexform['data']['sDEF']['lDEF'];
                $result = array_reduce(array_keys($flexform), function($result, $key) use ($flexform) {
                    $result[$key] = $flexform[$key]['vDEF'];
                    return $result;
                }, array());

                $content = '<p><b>' . $result['name'] . '</b><br>';
                if (!empty($result['description'])) {
                    $content .= '<span>' . $result['description'] . '</span>';
                }
                $content .= '</p>';

                if (!empty($result['email']) || !empty($result['phone'])) {
                    $content .= '<p>';
                    if (!empty($result['email'])) {
                        $content .= '<b>E-Mail:</b> ' . $result['email'] . '<br>';
                    }
                    if (!empty($result['phone'])) {
                        $content .= '<b>Telefon:</b> ' . $result['phone'];
                    }
                    $content .= '</p>';
                }
            }
            $itemContent .= $parentObject->linkEditContent($content, $row) . '<br>';
            if($row['assets']) {
                $itemContent .= $parentObject->linkEditContent($parentObject->getThumbCodeUnlinked($row, 'tt_content', 'assets'), $row) . '<br />';
            }
        }
        $drawItem = false;
    }
}